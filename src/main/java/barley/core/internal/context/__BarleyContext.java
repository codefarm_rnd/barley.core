package barley.core.internal.context;

import java.security.Principal;

import barley.core.internal.context.__BarleyContextHolder;

/**
 * This provides the API for thread local based programming for carbon based products. Each CarbonContext will utilize
 * an underlying {@link org.wso2.carbon.kernel.internal.context.CarbonContextHolder} instance, which will store the
 * actual data.
 *
 * @since 5.1.0
 */
@Deprecated
public class __BarleyContext {
    private __BarleyContextHolder carbonContextHolder = null;

    /**
     * Making this private so that it will not be instantiated.
     */
    private __BarleyContext() {
    }

    /**
     * Package local level constructor using a {@link CarbonContextHolder} instance in constructing this
     * CarbonContext instance.
     *
     * @param carbonContextHolder the carbonContextHolder instance to be used in creating a CarbonContext instance.
     */
    __BarleyContext(__BarleyContextHolder carbonContextHolder) {
        this.carbonContextHolder = carbonContextHolder;
    }

    /**
     * Returns the current carbon context holder associated with this CarbonContext instance.
     *
     * @return the carbon context holder instance.
     */
    __BarleyContextHolder getCarbonContextHolder() {
        return carbonContextHolder;
    }

    /**
     * Returns the carbon context instance which is stored at current thread local space.
     *
     * @return the carbon context instance.
     */
    public static __BarleyContext getCurrentContext() {
        return new __BarleyContext(__BarleyContextHolder.getCurrentContextHolder());
    }

    /**
     * The current jass user principal set with this carbon context instance. If no principal is set, a null value will
     * be returned.
     *
     * @return the jass user principal in the carbon context, null if no value is already set.
     */
    public Principal getUserPrincipal() {
        return getCarbonContextHolder().getUserPrincipal();
    }

    /**
     * Method the lookup currently stored property with the carbon context instance using the given property key name.
     *
     * @param name property key name to lookup.
     * @return the value stored using the given key, or null if no value is already set.
     */
    public Object getProperty(String name) {
        return getCarbonContextHolder().getProperty(name);
    }
}