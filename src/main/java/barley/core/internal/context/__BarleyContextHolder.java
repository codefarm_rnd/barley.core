package barley.core.internal.context;


import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This class will preserve an instance the current CarbonContextHolder as a thread local variable.
 * If a CarbonContextHolder is available on a thread-local-scope this class will do the required lookup and obtain
 * the corresponding instance.
 *
 * @since 5.0.0
 */
@Deprecated
public final class __BarleyContextHolder {

    private Principal userPrincipal;
    private Map<String, Object> properties = new HashMap<>();

    private static ThreadLocal<__BarleyContextHolder> currentContextHolder = new ThreadLocal<__BarleyContextHolder>() {
        protected __BarleyContextHolder initialValue() {
            return new __BarleyContextHolder();
        }
    };

    /**
     * Private Constructor which gets invoked via the static variable above. This gets invoked only once.
     */
    private __BarleyContextHolder() {
    }

    /**
     * Method to obtain the current thread local CarbonContextHolder instance.
     *
     * @return the thread local CarbonContextHolder instance.
     */
    public static __BarleyContextHolder getCurrentContextHolder() {
        return currentContextHolder.get();
    }

    /**
     * This method will destroy the current thread local CarbonContextHolder.
     */
    public void destroyCurrentCarbonContextHolder() {
        currentContextHolder.remove();
    }

    /**
     * Method to obtain a property on this CarbonContext instance.
     *
     * @param name the property name.
     * @return the value of the property by the given name.
     */
    public Object getProperty(String name) {
        return properties.get(name);
    }

    /**
     * Method to set a property on this CarbonContext instance.
     *
     * @param name  the property name.
     * @param value the value to be set to the property by the given name.
     */
    public void setProperty(String name, Object value) {
        properties.put(name, value);
    }

    /**
     * Method to obtain the currently set user principal from the CarbonContext instance.
     *
     * @return current user principal instance.
     */
    public Principal getUserPrincipal() {
        return userPrincipal;
    }

    /**
     * Checks and sets the given user principal to the current context.
     *
     * @param userPrincipal the user principal to be set
     */
    public void setUserPrincipal(Principal userPrincipal) {
        if (this.userPrincipal == null) {
            this.userPrincipal = userPrincipal;
        } else {
            Optional.ofNullable(this.userPrincipal.getName())
                    .filter(name -> userPrincipal.getName().equals(name))
                    .orElseThrow(() -> new IllegalStateException("Trying to override the already available user " +
                            "principal from " + this.userPrincipal.toString() + " to " +
                            userPrincipal.toString()));
        }
    }
}