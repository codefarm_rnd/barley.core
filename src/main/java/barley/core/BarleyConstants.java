/*
 * Copyright 2004,2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package barley.core;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *  This class contains all the significant constants used by Carbon core & Carbon components
 */
public final class BarleyConstants {

	// (신규추가)
	public static final Log AUDIT_LOG = LogFactory.getLog("AUDIT_LOG");
	
    /**
     * This is used to get root context within CarbonJNDIContext when we need to operate
     * with LDAP.
     */
    public static String REQUEST_BASE_CONTEXT = "org.wso2.carbon.context.RequestBaseContext";

    public static final String REGISTRY_SYSTEM_USERNAME = "barley.system.user";
    
    public static final String REGISTRY_ANONNYMOUS_USERNAME = "barley.anonymous.user";
    public static final String REGISTRY_ANONNYMOUS_ROLE_NAME = "barley.anonymous.role";

    public static final String UI_PERMISSION_NAME = "permission";
    public static final String UI_PERMISSION_COLLECTION = "/" + UI_PERMISSION_NAME;
    public static String UI_ADMIN_PERMISSION_COLLECTION = UI_PERMISSION_COLLECTION + "/admin";
    public static String UI_PROTECTED_PERMISSION_COLLECTION = UI_PERMISSION_COLLECTION + "/protected";
    public static String UI_PERMISSION_ACTION = "ui.execute";
    public static final String UI_USER_PERMISSIONS = "user-permissions";

    public static final String AUTHZ_FAULT_CODE = "WSO2CarbonAuthorizationFailure";
    public static final String MODULE_NOT_FOUND_FAULT_CODE = "Axis2ModuleNotFound";


    public static final String START_TIME = "wso2carbon.start.time";

    public final static String COMMAND_EXT = ".command";
    public final static String WSO2CARBON_NS = "http://products.wso2.org/carbon";
    public final static String COMPONENT_XML = "component.xml";
    public final static String UI_FILE = "ui";

    public final static String COMPONENT_ELE = "component";
    public final static String TAG_LIBS_ELE = "taglibs";
    public final static String TAG_LIB_ELE = "taglib";
    public final static String JS_FILES_ELE = "js-files";
    public final static String JS_FILE_ELE = "js-file";

    public final static String URL_ATTR = "url";
    public final static String PREFIX_ATTR = "prefix";

    public final static String GENERAL_ELE = "general";

    public final static String MENUS_ELE = "menus";
    public final static String MENUE_ELE = "menu";
    //ADD MENUS is used when component is in RESOLVE/INSTALLED phase
    //REMOVE MENUS is used when component is being UNINSTALLED/STOPPED
//    public final static String ADD_MENUS = "add-menus";
//    public final static String REMOVE_MENUS = "remove-menus";

    public final static String ADD_UI_COMPONENT = "add-ui-component";
    public final static String REMOVE_UI_COMPONENT = "remove-ui-component";

    public final static String ACTION_REF_ATTR = "action-ref";
    public final static String LEVEL_ATTR = "level";
    public final static String ACTION_ELE = "action";
    public final static String ACTIONS_ELE = "actions";
    public final static String VIEW_ATTR = "view";
    public final static String CLASS_ATTR = "class";
    public final static String METHOD_ATTR = "method";
    public final static String METHOD_ATTR_DEFAULT_VALUE = "execute";

    public final static String RELATIVE_TEMPLATE_LOCATION = "repository" + File.separator + "conf" + File.separator + "templates";

    public final static String HEADER_XSL = "header.xsl";
    public final static String INDEX_XSL = "index.xsl";
    public final static String MENU_XSL = "menue.xsl";

    public final static String NAME_ATTR = "name";

    public final static String HTTP_TRANSPORT = "http";
    public final static String HTTPS_TRANSPORT = "https";

    public final static String HOST_NAME = "host-name";

    public static final String KEY_REGISTRY_INSTANCE = "WSO2Registry";

    public static final String WEB_CONTEXT = "WebContext";

    //Remember me
    public static final String REMEMBER_ME_COOKIE_VALUE = "wso2.carbon.rememberme.value";
    public static final String REMEMBER_ME_COOKIE_AGE = "wso2.carbon.rememberme.age";
    public static final String REMEMBER_ME_COOKE_NAME = "wso2.carbon.rememberme";
    public static final int REMEMBER_ME_COOKIE_TTL = 604800; //in seconds // 7 days
    
    //Names of Attributes that are set in the ServletContext
    public static final String CONFIGURATION_CONTEXT = "ConfigurationContext";
    public static final String CLIENT_CONFIGURATION_CONTEXT = "ClientConfigurationContext";

    //To mark whether the UI framewrok in running on local transport mode.
    public static final String LOCAL_TRANSPORT_MODE_ENABLED = "localTransportModeEnabled";    

    public static final String SERVER_URL = "ServerURL";
    public static final String INDEX_PAGE_URL = "IndexPageURL";
    public static final String DEFAULT_HOME_PAGE = "defaultHomePage";
    public static final String REGISTRY = "registry";
    public static final String BUNDLE_CLASS_LOADER = "BundleClassLoader";

    public static final String ADMIN_SERVICE_COOKIE = "wso2carbon.admin.service.cookie";

    //Constants used in File Uploading
    public static final String FILE_UPLOAD_CONFIG = "FileUploadConfig";
    public static final String MAPPING = "Mapping";
    public static final String ACTIONS = "Actions";
    public static final String ACTION = "Action";
    public static final String CLASS = "Class";

    //Constants used for servlet definition in component.xml
    public static final String SERVLETS = "servlets";
    public static final String SERVLET = "servlet";
    public static final String SERVLET_ID = "id";
    public static final String SERVLET_NAME = "servlet-name";
    public static final String SERVLET_DISPLAY_NAME = "display-name";
    public static final String SERVLET_URL_PATTERN = "url-pattern";
    public static final String SERVLET_ATTRIBUTES = "servlet-attribute";
    public static final String ASSOCIATED_FILTER = "associated-filter";
    public static final String SERVLET_PARAMS = "servlet-params";
    public static final String SERVLET_CLASS = "servlet-class";
    public final static String ADD_SERVLET = "add-servlet";
    public final static String REMOVE_SERVLET = "remove-servlet";

    //Constants useing for framework config
    public final static String FRAMEWORK_CONFIG = "framework-configuration";
    public final static String BYPASS = "bypass";
    public final static String AUTHENTICATION = "authentication";
    public final static String LINK = "link";
    public final static String TILES = "tiles";
    public final static String HTTP_URLS = "httpUrls";

    // Ghost Deployment related constants
    public final static String GHOST_DEPLOYER = "ghostDeployer";
    public static final String GHOST_SERVICE_PARAM = "GhostService";
    public static final String GHOST_SERVICES_FOLDER = "ghostServices";
    public static final String GHOST_SERVICE_GROUP = "serviceGroup";
    public static final String GHOST_SERVICE = "service";
    public static final String GHOST_ATTR_NAME = "name";
    public static final String GHOST_ATTR_SERVICE_TYPE = "serviceType";
    public static final String GHOST_ATTR_MEP = "mep";
    public static final String GHOST_ATTR_SECURITY_SCENARIO = "securityScenario";
    public static final String GHOST_SERVICE_OPERATIONS = "operations";
    public static final String GHOST_SERVICE_ENDPOINTS = "endpoints";

    public static final String SERVICE_DEPLOYMENT_TIME_PARAM = "serviceDeploymentTime";

    public static final String SERVICE_LAST_USED_TIME = "lastUsedTime";

    /**
     * The allowed idle time for a service deployed
     */
    public static final String SERVICE_IDLE_TIME = "service.idle.time";
    public static final int SERVICE_CLEANUP_PERIOD_SECS = 60;

    //Constants useing for context config
    public final static String CONTEXTS = "contexts";
    public final static String CONTEXT = "context";
    public final static String CONTEXT_ID = "context-id";
    public final static String CONTEXT_NAME = "context-name";
    public final static String PROTOCOL = "protocol";
    public final static String DESCRIPTION = "description";

    public static final String ADMIN_SERVICE_PARAM_NAME = "adminService";
    public static final String HIDDEN_SERVICE_PARAM_NAME = "hiddenService";
    public static final String DYNAMIC_SERVICE_PARAM_NAME = "dynamicService";
    public static final String ADMIN_MODULE_PARAM_NAME = "adminModule";
    public static final String MANAGED_MODULE_PARAM_NAME = "managedModule";

    //Constants used for menu definition in component.xml
    public final static String REQUIRE_PERMISSION = "require-permission";
    public final static String REQUIRE_NOT_LOGGED_IN = "require-not-logged-in";

    public static final String THEME_URL_RANDOM_SUFFIX_SESSION_KEY = "theme-suffix";

    //multi tenant related details

    /**
     * @deprecated use MultitenantConstants.REQUIRE_SUPER_TENANT
     */
    @Deprecated
    public final static String REQUIRE_SUPER_TENANT = MultitenantConstants.REQUIRE_SUPER_TENANT;
    /**
     * @deprecated use MultitenantConstants.REQUIRE_NOT_SUPER_TENANT
     */
    @Deprecated
    public final static String REQUIRE_NOT_SUPER_TENANT = MultitenantConstants.REQUIRE_NOT_SUPER_TENANT;
    /**
     * @deprecated use MultitenantConstants.TENANT_DOMAIN
     */
    @Deprecated
    public static final String TENANT_DOMAIN = MultitenantConstants.TENANT_DOMAIN;
    /**
     * @deprecated use MultitenantConstants.TENANT_AWARE_URL_PREFIX
     */
    @Deprecated
    public static final String TENANT_AWARE_URL_PREFIX = MultitenantConstants.TENANT_AWARE_URL_PREFIX;
    /**
     * @deprecated use MultitenantConstants.SUPER_TENANT_ID
     */
    @Deprecated
    public static final int SUPER_TENANT_ID = MultitenantConstants.SUPER_TENANT_ID;
    /**
     * @deprecated use MultitenantConstants.TENANT_DOMAIN_HEADER_NAMESPACE
     */
    @Deprecated
    public static final String TENANT_DOMAIN_HEADER_NAMESPACE = MultitenantConstants.TENANT_DOMAIN_HEADER_NAMESPACE;
    /**
     * @deprecated use MultitenantConstants.TENANT_DOMAIN_HEADER_NAME
     */
    @Deprecated
    public static final String TENANT_DOMAIN_HEADER_NAME = MultitenantConstants.TENANT_DOMAIN_HEADER_NAME;
    /**
     * @deprecated use MultitenantConstants.SUPER_TENANT_DOMAIN
     */
    @Deprecated
    public static final String SUPER_TENANT_DOMAIN = MultitenantConstants.SUPER_TENANT_DOMAIN;


    public final static String NAME_REGULAR_EXPRESSION = "^[^~!@#$;%^*+={}\\|\\\\<>]{3,30}$";
    //Axis2 related constants.
    public static final String AXIS2_CONFIG_SERVICE = "org.apache.axis2.osgi.config.service";
    public static String AXIS2_WS = "org.apache.axis2.osgi.ws";
    public static int POLICY_ADDED = -1;
    public static String HTTP_GET_REQUEST_PROCESSOR_SERVICE = "org.wso2.carbon.osgi.httpGetRequestProcessorService";

    // A Map to store the faulty services in the Axis2 ConfigurationContext
    public static final String FAULTY_SERVICES_MAP = "local_carbon.faulty.services.map";

    public static final String KEEP_SERVICE_HISTORY_PARAM = "keepServiceHistory";
    public static final String PRESERVE_SERVICE_HISTORY_PARAM = "preserveServiceHistory";

    /**
     * The Carbon UI bundle context
     */
    public static final String UI_BUNDLE_CONTEXT = "carbon.ui.bundle.context";

    public static final String SERVER_START_TIME = "wso2carbon.server.start.time";

    /**
     * Location where the Web resources within AAR files are expanded into
     */
    public static final String WEB_RESOURCE_LOCATION = "web.location";
    public static final String AXIS2_CONFIG_PARAM = "Axis2Config";

    //Permissions to a Role

    public static class Permission {
        public static final String LOGIN_TO_ADMIN_UI = "Login to Admin UI";
        public static final String MANAGE_SYSTEM_CONFIGURATION = "Manage system configuration";
        public static final String MANAGE_SECURITY = "Manage security";
        public static final String UPLOAD_SERVICES = "Upload Service";
        public static final String MANAGE_SERVICES = "Manage Services";
        public static final String MANAGE_MEDIATION = "Manage Mediation";
    }

    public final static String PRODUCT_XML = "product.xml";
    public final static String PRODUCT_XML_WSO2CARBON = "WSO2Carbon";
    public final static String PRODUCT_XML_PROPERTY = "property";
    public final static String PRODUCT_XML_PROPERTIES = "properties";
    public final static String PRODUCT_XML_USERFORUM = "userforum";
    public final static String PRODUCT_XML_USERGUIDE = "userguide";
    public final static String PRODUCT_XML_MAILINGLIST = "mailinglist";
    public final static String PRODUCT_XML_ISSUETRACKER = "issuetracker";
    public final static String PRODUCT_XML_WEB_ADMIN_CONSOLE_TITLE = "webAdminConsoleTitle";

    public final static String PRODUCT_STYLES_CONTEXT = "styles";

    public final static String CARBON_FAULTY_SERVICE = "carbonFaultyService";
    public final static String CARBON_FAULTY_SERVICE_DUE_TO_MODULE =
            "This service is cannot be started due to missing modules";

    public static class CarbonManifestHeaders {
        public final static String AXIS2_MODULE = "Axis2Module";
        public final static String AXIS2_DEPLOYER = "Axis2Deployer";
        public final static String AXIS2_INIT_REQUIRED_SERVICE = "Axis2RequiredServices";
        public final static String LISTENER_MANAGER_INIT_REQUIRED_SERVICE =
                "ListenerManager-RequiredServices";
    }

    public static final String CARBON_HOME_ENV = "CARBON_HOME";
    public static final String CARBON_HOME_PARAMETER = "${carbon.home}";
    public static final String CARBON_CONFIG_DIR_PATH_ENV = "CARBON_CONFIG_DIR_PATH";
    public static final String CARBON_TENANTS_DIR_PATH_ENV = "CARBON_TENANTS_DIR_PATH";
    public static final String CARBON_LOGS_PATH_ENV = "CARBON_LOGS";
    public static final String AXIS2_REPO_ENV = "AXIS2_REPO";
    public static final String COMPONENT_REP0_ENV = "COMPONENTS_REPO";
    
    public static final String AUTHENTICATOR_TYPE = "authenticator.type";
    
    // A Service group parameter
    public static final String FORCE_EXISTING_SERVICE_INIT = "forceExistingServiceInit";

    /**
     * Name of the property which is used for storing the WebApplicationsHolder
     */
    public static final String WEB_APPLICATIONS_HOLDER = "carbon.webapps.holder";

    /**
     * Name of the property to hold the servletContextParameters list
     */
    public static final String SERVLET_CONTEXT_PARAMETER_LIST = "servlet.context.parameters.list";

    /**
     * The attribute stored at UI session
     */
    public static final String LOGGED_USER = "logged-user";
    
    /**
     * Custom Axis2 events
     **/
    public static class AxisEvent {
        public static final int TRANSPORT_BINDING_ADDED = 100;
        public static final int TRANSPORT_BINDING_REMOVED = 101;
    }

    public static final String USERNAME_STYLE = "UserName.UserNameStyle";
    public static final String USERNAME_STYLE_VALUE_EMAIL = "Email";
    public static final String USERNAME_STYLE_VALUE_PLAIN = "TruncatedDomain";

    public static final String REGISTRY_HTTP_PORT = "RegistryHttpPort";

    public static final String HIDE_ADMIN_SERVICE_WSDLS = "HideAdminServiceWSDLs";

    public static final String CARBON_UI_DEFAULT_HOME_PAGE = "../admin/index.jsp";
    
    public static final String CARBON_ADMIN_SERVICE_PERMISSIONS = "carbon.permissions";

    
    /*Constants used in handling multiple user store operations*/
    // https://svn.wso2.org/repos/wso2/carbon/kernel/branches/4.0.0/core/org.wso2.carbon.utils/4.0.7/src/main/java/org/wso2/carbon/CarbonConstants.java
    // 참고해서 추가함. 
    public static final String DOMAIN_SEPARATOR = "/";
    public static final String NAME_COMBINER = "|";
    
}