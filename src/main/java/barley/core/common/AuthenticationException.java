package barley.core.common;

public class AuthenticationException extends Exception {

    private String uiErrorCode;

    public AuthenticationException() {
        super();
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationException(String message, Throwable cause, String code) {
        super(message, cause);
        this.uiErrorCode = code;
    }

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(Throwable cause) {
        super(cause);
    }

    public String getUiErrorCode() {
        return uiErrorCode;
    }

    public void setUiErrorCode(String uiErrorCode) {
        this.uiErrorCode = uiErrorCode;
    }
}