package barley.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class PasswordPrompt implements Runnable {
    private static final int PROMPT_CLEARANCE_LENGTH = 10;
	private volatile boolean done = false;
    private String prompt;
    private PrintWriter out;

    public PasswordPrompt(String prompt, PrintWriter out) {
        this.prompt = prompt;
        this.out = out;
    }

    public void run() {
        int priority = Thread.currentThread().getPriority();
        try {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            String fullPrompt = "\r" + prompt + "          " + "\r" + prompt;
            StringBuffer clearline = new StringBuffer();
            clearline.append('\r');
            for (int i = prompt.length() + PROMPT_CLEARANCE_LENGTH; i >= 0; i--) {
                clearline.append(' ');
            }
            while (!done) {
                out.print(fullPrompt);
                out.flush();
                Thread.sleep(1);
            }
            out.print(clearline.toString());
            out.flush();
            out.println();
            out.flush();
        } catch (InterruptedException e) {
        } finally {
            Thread.currentThread().setPriority(priority);
        }
        prompt = null;
        out = null;
    }

    public String getPassword(BufferedReader in) throws IOException {
        Thread t = new Thread(this, "Password hiding thread");
        t.start();
        String password = in.readLine();
        done = true;
        try {
            t.join();
        } catch (InterruptedException e) {
        }
        return password;
    }
}