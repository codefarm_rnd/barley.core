package barley.core.utils;

import org.apache.axis2.context.ConfigurationContext;

public class ConfigurationContextService {
    private ConfigurationContext serverConfigContext;
    private ConfigurationContext clientConfigContext;

    public ConfigurationContextService(ConfigurationContext serverConfigContext, ConfigurationContext clientConfigContext){
        this.serverConfigContext = serverConfigContext;
        this.clientConfigContext = clientConfigContext;
    }

    public ConfigurationContext getServerConfigContext() {
        return serverConfigContext;
    }

    public ConfigurationContext getClientConfigContext() {
        return clientConfigContext;
    }
}