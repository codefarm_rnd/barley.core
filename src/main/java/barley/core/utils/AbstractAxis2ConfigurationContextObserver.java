package barley.core.utils;

import org.apache.axis2.context.ConfigurationContext;

/**
 * This is an abstract implementation of the Axis2ConfigurationContextObserver interface. It
 * does not perform any action on the events fired by the above interface and exists for the
 * sole purpose of providing an extension point.
 */
public abstract class AbstractAxis2ConfigurationContextObserver implements
        Axis2ConfigurationContextObserver {

    public void creatingConfigurationContext(int tenantId) {

    }

    public void createdConfigurationContext(ConfigurationContext configContext) {

    }

    public void terminatingConfigurationContext(ConfigurationContext configCtx) {

    }

    public void terminatedConfigurationContext(ConfigurationContext configCtx) {

    }
}