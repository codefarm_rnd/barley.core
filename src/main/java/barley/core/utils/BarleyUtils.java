package barley.core.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.lang.management.ManagementPermission;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axiom.util.base64.Base64Utils;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.description.Parameter;
import org.apache.axis2.description.TransportInDescription;
import org.apache.axis2.engine.AxisConfiguration;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.Header;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import barley.core.BarleyConstants;
import barley.core.BarleyException;
import barley.core.ServerConstants;
import barley.core.configuration.ServerConfiguration;
import barley.core.configuration.ServerConfigurationService;

public class BarleyUtils {

	private static final boolean isWorkerNode = Boolean.parseBoolean(System.getProperty("workerNode"));
	
	private static final String REPOSITORY = "repository";
    private static final String TRANSPORT_MANAGER = "org.wso2.carbon.tomcat.ext.transport.ServletTransportManager";
	private static final String TRUE = "true";
	private static Log log = LogFactory.getLog(BarleyUtils.class);
    private static boolean isServerConfigInitialized;

    /**
     * Check if the current server is a worker node
     * In clustering setup some servers are run as worker nodes and some as management nodes
     * @return
     */
    public static boolean isWorkerNode() {
        return isWorkerNode;
    }
    
    public static String getCarbonConfigDirPath() {
        String carbonConfigDirPath = System.getProperty(ServerConstants.CARBON_CONFIG_DIR_PATH);
        if (carbonConfigDirPath == null) {
            carbonConfigDirPath = System.getenv(BarleyConstants.CARBON_CONFIG_DIR_PATH_ENV);
            if (carbonConfigDirPath == null) {
                return getCarbonHome() + File.separator + "repository" + File.separator + "conf";
            }
        }
        return carbonConfigDirPath;
    }

    // (수정) 2019.09.19 - 시스템 변수 공유는 많은 문제를 발생한다. 따라서 각 프로젝트 클래스 위치에서 찾도록 변경한다.   
    public static String getCarbonHome() {
        String carbonHome = System.getProperty(ServerConstants.CARBON_HOME);
        if (carbonHome == null) {
        	/*
            carbonHome = System.getenv(BarleyConstants.CARBON_HOME_ENV);
            System.setProperty(ServerConstants.CARBON_HOME, carbonHome);
        	 */
        	//carbonHome = BarleyUtils.class.getResource("/").getPath();
        	URL url = MethodHandles.lookup().lookupClass().getResource("/");
        	if(url != null) {
        		carbonHome = url.getPath();
        	}
        	
        	// get project root path 
        	if(carbonHome == null) {
        		carbonHome = System.getProperty("user.dir");
        	}
        }
        return carbonHome;	
    }
    
    public static boolean isRemoteRegistry() throws Exception {
        ServerConfiguration serverConfig = getServerConfiguration();
        String isRemoteRegistry =
                serverConfig.getFirstProperty("Registry.Type");
        return (isRemoteRegistry != null && isRemoteRegistry.equalsIgnoreCase("remote"));
    }
    
    public static ServerConfiguration getServerConfiguration() {
        ServerConfiguration serverConfig = ServerConfiguration.getInstance();
        if (!isServerConfigInitialized) {
            String serverXml = BarleyUtils.getServerXml();
            File carbonXML = new File(serverXml);
            InputStream inSXml = null;
            try {
                inSXml = new FileInputStream(carbonXML);
                serverConfig.init(inSXml);
                isServerConfigInitialized = true;
            } catch (Exception e) {
                log.error("Cannot read file " + serverXml, e);
            } finally {
                if (inSXml != null) {
                    try {
                        inSXml.close();
                    } catch (IOException e) {
                        log.warn("Cannot close file " + serverXml, e);
                    }
                }
            }
        }
        return serverConfig;
    }
    
    public static String getServerXml() {
        String carbonXML =
                System.getProperty(ServerConstants.CARBON_CONFIG_DIR_PATH);
        /* if user set the system property telling where is the configuration directory*/
        if (carbonXML == null) {
            return getCarbonConfigDirPath() + File.separator + "carbon.xml";
        }
        return carbonXML + File.separator + "carbon.xml";
    }    
    
    public static String getCarbonTenantsDirPath() {
        String carbonTenantsDirPath = System.getProperty(ServerConstants.CARBON_TENANTS_DIR_PATH);
        if (carbonTenantsDirPath == null) {
            carbonTenantsDirPath = System.getenv(BarleyConstants.CARBON_TENANTS_DIR_PATH_ENV);
        }
        if (carbonTenantsDirPath == null) {
            carbonTenantsDirPath = getCarbonHome() + File.separator + REPOSITORY +
                    File.separator + "tenants";
        }
        return carbonTenantsDirPath;
    }
    
    public static boolean isReadOnlyNode() {
        if (isChildNode() && !isMasterNode()) {
            return true;
        }
        return false;
    }
    
    public static boolean isChildNode() {
        if (TRUE.equals(System.getProperty("instance"))) {
            return true;
        }
        return false;
    }
    
    public static boolean isMasterNode() {
        if (TRUE.equals(System.getProperty("instance")) && TRUE.equals(System.getProperty("master"))) {
            return true;
        }
        return false;
    }
    
    public static String getRegistryXMLPath() {
        String carbonHome = BarleyUtils.getCarbonHome();
        String configPath = null;
        if (carbonHome != null) {
            if (System.getProperty(ServerConstants.REGISTRY_XML_PATH) == null) {
                configPath = BarleyUtils.getCarbonConfigDirPath() + File.separator + "registry.xml";
            } else {
                configPath = System.getProperty(ServerConstants.REGISTRY_XML_PATH);
            }
        }
        return configPath;
    }
    
    public static String getUserMgtXMLPath() {
        String carbonHome = BarleyUtils.getCarbonHome();
        String configPath = null;
        if (carbonHome != null) {
            if (System.getProperty(ServerConstants.USER_MGT_XML_PATH) == null) {
                configPath = BarleyUtils.getCarbonConfigDirPath() + File.separator + "user-mgt.xml";
            } else {
                configPath = System.getProperty(ServerConstants.USER_MGT_XML_PATH);
            }
        }
        return configPath;
    }

    /**
     * This is to read the port values defined in other config files, which are overridden
     * from those in carbon.xml.
     * @param property
     * @return
     */
    public static int getPortFromServerConfig(String property) {
        String port;
        int portNumber = -1;
        int indexOfStartingChars = -1;
        int indexOfClosingBrace;

        ServerConfiguration serverConfiguration = ServerConfiguration.getInstance();
        // The following condition deals with ports specified to be read from carbon.xml.
        // Ports are specified as templates: eg ${Ports.EmbeddedLDAP.LDAPServerPort},
        if (indexOfStartingChars < property.indexOf("${") &&
            (indexOfStartingChars = property.indexOf("${")) != -1 &&
            (indexOfClosingBrace = property.indexOf('}')) != -1) { // Is this template used?

            String portTemplate = property.substring(indexOfStartingChars + 2,
                                                     indexOfClosingBrace);

            port = serverConfiguration.getFirstProperty(portTemplate);

            if (port != null) {
                portNumber = Integer.parseInt(port);
            }

        }
        String portOffset = System.getProperty("portOffset",
                                               serverConfiguration.getFirstProperty("Ports.Offset"));
        //setting up port offset properties as system global property which allows this
        //to available at the other context as required (fix 2011-11-30)
        System.setProperty("portOffset", portOffset);
        return portOffset == null? portNumber : portNumber + Integer.parseInt(portOffset);
    }
    

    // api.impl에서 필요해 새로 추가함 
    // 출처: https://github.com/yasassri/carbon4-kernel/blob/master/core/org.wso2.carbon.utils/src/main/java/org/wso2/carbon/utils/CarbonUtils.java
    public static void setBasicAccessSecurityHeaders(String userName, String password, ServiceClient serviceClient) {
        setBasicAccessSecurityHeaders(userName, password, false, serviceClient);
    }
    
    public static void setBasicAccessSecurityHeaders(String userName, String password, boolean rememberMe, ServiceClient serviceClient) {

		String userNamePassword = userName + ":" + password;
		String encodedString = Base64Utils.encode(userNamePassword.getBytes());
		
		String authorizationHeader = "Basic " + encodedString;
		
		List<Header> headers = new ArrayList<Header>();
		
		Header authHeader = new Header("Authorization", authorizationHeader);
		headers.add(authHeader);
		
		if (rememberMe) {
			Header rememberMeHeader = new Header("RememberMe", TRUE);
			headers.add(rememberMeHeader);
		}
		
		serviceClient.getOptions().setProperty(HTTPConstants.HTTP_HEADERS, headers);
	}
    
    public static void setBasicAccessSecurityHeaders(String userName, String password, boolean rememberMe, MessageContext msgContext) 
    		throws AxisFault {

		String userNamePassword = userName + ":" + password;
		String encodedString = Base64Utils.encode(userNamePassword.getBytes());
		
		String authorizationHeader = "Basic " + encodedString;
		
		List<Header> headers = new ArrayList<Header>();
		
		Header authHeader = new Header("Authorization", authorizationHeader);
		headers.add(authHeader);
		
		if (rememberMe) {
		Header rememberMeHeader = new Header("RememberMe", TRUE);
		headers.add(rememberMeHeader);
		}
		
		msgContext.getOptions().setProperty(HTTPConstants.HTTP_HEADERS, headers);
	}
    
    public static int getTransportProxyPort(ConfigurationContext configContext, String transport) {
        return getTransportProxyPort(configContext.getAxisConfiguration(), transport);
    }
    
    
    // api.impl에서 필요해 새로 추가함
    // https://github.com/yasassri/carbon4-kernel/blob/master/core/org.wso2.carbon.utils/src/main/java/org/wso2/carbon/utils/CarbonUtils.java
    /**
     * Get the proxy port corresponding to a particular transport
     *
     * @param axisConfig The AxisConfiguration
     * @param transport  The transport
     * @return The proxy port corresponding to the <code>transport</code>.
     *         -1 if this parameter is not defined
     * @throws IllegalStateException If no port is assocaited with the transport, or if this method
     *                               has been called before the ListenerManager has started
     */
    public static int getTransportProxyPort(AxisConfiguration axisConfig, String transport) {
        int proxyPort = -1;
        try {
            proxyPort = readProxyPort(transport);  //read proxyPort from ServletTransportManager
        } catch (BarleyException e) {
            //logging error and continue
            //Exceptions in here, are due to issues with reading proxyPort
            log.error("Error reading proxyPort value ", e);
        }
        return proxyPort;
    }
    
    private static int readProxyPort(String transport) throws BarleyException {
        int proxyPort;
        Class[] paramTypes = new Class[1];
        paramTypes[0] = String.class;
        try {
            Class<?> transportManagerClass = Class.forName(TRANSPORT_MANAGER);
            Object transportManager = transportManagerClass.newInstance();
            Method method = transportManagerClass.getMethod("getProxyPort", paramTypes);
            proxyPort = (Integer) method.invoke(transportManager, transport);
        } catch (ClassNotFoundException e) {
            throw new BarleyException("No class found with name : " + TRANSPORT_MANAGER, e);
        } catch (InstantiationException e) {
            throw new BarleyException("Error creating instance for : " + TRANSPORT_MANAGER, e);
        } catch (IllegalAccessException e) {
            throw new BarleyException("Error creating instance for : " + TRANSPORT_MANAGER, e);
        } catch (NoSuchMethodException e) {
            throw new BarleyException("No method found with name : getProxyPort(String param)", e);
        } catch (InvocationTargetException e) {
            throw new BarleyException("Error invoking method : getProxyPort() , with param: " + transport, e);
        }
        return proxyPort;
    }

    /**
     * Get the port corresponding to a particular transport
     *
     * @param configContextService The OSGi ConfigurationContextService
     * @param transport            The transport
     * @return The port corresponding to the <code>transport</code>
     * @throws IllegalStateException If no port is associated with the transport, or if this method
     *                               has been called before the ListenerManager has started
     */
    /* (임시주석)
    public static int getTransportPort(ConfigurationContextService configContextService,
                                       String transport) {
        return getTransportPort(configContextService.getServerConfigContext(), transport);
    }
    */
    
    /**
     * Get the port corresponding to a particular transport
     *
     * @param configContext The Axis2 ConfigurationContext
     * @param transport     The transport
     * @return The port corresponding to the <code>transport</code>
     * @throws IllegalStateException If no port is assocaited with the transport, or if this method
     *                               has been called before the ListenerManager has started
     */
    public static int getTransportPort(ConfigurationContext configContext, String transport) {
        return getTransportPort(configContext.getAxisConfiguration(), transport);
    }
    
    /**
     * Get the port corresponding to a particular transport
     *
     * @param axisConfig The AxisConfiguration
     * @param transport  The transport
     * @return The port corresponding to the <code>transport</code>
     * @throws IllegalStateException If no port is assocaited with the transport, or if this method
     *                               has been called before the ListenerManager has started
     */
    public static int getTransportPort(AxisConfiguration axisConfig, String transport) {
        String transportPort = System.getProperty(transport + "Port");
        if (transportPort == null) {
            if(axisConfig == null) return -1; // Server not yet started
            TransportInDescription transportIn = axisConfig.getTransportIn(transport);
            if(transportIn == null) return -1; // Transport not yet started
            Parameter portParam = transportIn.getParameter("port");
            if (portParam != null) {
                transportPort = (String) portParam.getValue();
            } else {
                throw new IllegalStateException(transport + " port has not been set yet. " +
                        "Most probably the ListenerManager has not" +
                        " been started yet or the this transport does not " +
                        "have a port associated with it. You can wait until " +
                        "the org.apache.axis2.engine.ListenerManager OSGi " +
                        "service is available & retry.");
            }
        }
        return Integer.parseInt(transportPort);
    }

    /**
     * @return The transport on which the management functionality is available
     */
    public static String getManagementTransport() {
        String mgtConsoleTransport =
                ServerConfiguration.getInstance().getFirstProperty("ManagementTransport");
        if (mgtConsoleTransport == null || mgtConsoleTransport.startsWith("${")) {
            mgtConsoleTransport = "https";
        }
        return mgtConsoleTransport;
    }
    
    // (추가) 2017.10.11
    public static String getCarbonSecurityConfigDirPath() {
        return getCarbonConfigDirPath() + File.separator + "security";
    }
    
    /**
    * (추가) 2017.10.30
    * @param xmlConfiguration InputStream that carries xml configuration
    * @return returns a InputStream that has evaluated system variables in input
    * @throws CarbonException
    */
   public static InputStream replaceSystemVariablesInXml(InputStream xmlConfiguration) throws BarleyException {
       DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
       DocumentBuilder builder;
       Document doc;
       try {
           builder = factory.newDocumentBuilder();
           doc = builder.parse(xmlConfiguration);
       } catch (Exception e) {
           throw new BarleyException("Error in building Document", e);
       }
       NodeList nodeList = null;
       if (doc != null) {
           nodeList = doc.getElementsByTagName("*");
       }
       if (nodeList != null) {
           for (int i = 0; i < nodeList.getLength(); i++) {
               resolveLeafNodeValue(nodeList.item(i));
           }
       }
       return toInputStream(doc);
   }
   
   // (추가) 2017.10.11
   public static void resolveLeafNodeValue(Node node) {
       if (node != null) {
           Element element = (Element) node;
           NodeList childNodeList = element.getChildNodes();
           for (int j = 0; j < childNodeList.getLength(); j++) {
               Node chileNode = childNodeList.item(j);
               if (!chileNode.hasChildNodes()) {
                   String nodeValue = resolveSystemProperty(chileNode.getTextContent());
                   childNodeList.item(j).setTextContent(nodeValue);
               } else {
                   resolveLeafNodeValue(chileNode);
               }
           }
       }
   }
   
   // (추가) 2017.10.11
   public static String replaceSystemVariablesInXml(String xmlConfiguration) throws BarleyException {
       InputStream in = replaceSystemVariablesInXml(new ByteArrayInputStream(xmlConfiguration.getBytes()));
       try {
           xmlConfiguration = IOUtils.toString(in);
       } catch (IOException e) {
           throw new BarleyException("Error in converting InputStream to String");
       }
       return xmlConfiguration;
   }
   
   // (추가) 2017.10.11
   public static String resolveSystemProperty(String text) {
		int indexOfStartingChars = -1;
		int indexOfClosingBrace;

		// The following condition deals with properties.
		// Properties are specified as ${system.property},
		// and are assumed to be System properties
		while (indexOfStartingChars < text.indexOf("${")
				&& (indexOfStartingChars = text.indexOf("${")) != -1
				&& (indexOfClosingBrace = text.indexOf('}')) != -1) { // Is a
																		// property
																		// used?
			String sysProp = text.substring(indexOfStartingChars + 2,
					indexOfClosingBrace);
			String propValue = System.getProperty(sysProp);
			if (propValue != null) {
				text = text.substring(0, indexOfStartingChars) + propValue
						+ text.substring(indexOfClosingBrace + 1);
			}
			if (sysProp.equals("carbon.home") && propValue != null
					&& propValue.equals(".")) {

				text = new File(".").getAbsolutePath() + File.separator + text;

			}
		}
		return text;
	}
   
   /**
   * (추가) 2017.10.11
   * @param doc  the DOM.Document to be converted to InputStream.
   * @return Returns InputStream.
   * @throws CarbonException
   */
	public static InputStream toInputStream(Document doc) throws BarleyException {
		InputStream in;
	    try {
	    	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    	Source xmlSource = new DOMSource(doc);
	    	Result result = new StreamResult(outputStream);
	    	TransformerFactory.newInstance().newTransformer().transform(xmlSource, result);
	    	in = new ByteArrayInputStream(outputStream.toByteArray());
      	} catch (TransformerException e) {
      		throw new BarleyException("Error in transforming DOM to InputStream", e);
      	}
      	return in;
	}

	/**
     * When the java security manager is enabled, the {@code checkSecurity} method can be used to protect/prevent
     * methods being executed by unsigned code.
     * (추가) 2018.01.29 Utils에 있는 메소드를 여기로 이동함. 
     */
    public static void checkSecurity() {
        SecurityManager secMan = System.getSecurityManager();
        if (secMan != null) {
            secMan.checkPermission(new ManagementPermission("control"));
        }
    }

    // (츠가) 2019.11.20 
    public static String getServerURL(ServerConfigurationService serverConfig,
            ConfigurationContext configCtx) {
		ServerConfigurationService serverConfigToProcess = (serverConfig == null)? getServerConfiguration() : serverConfig ;
		
		return getServerURL(serverConfigToProcess.getFirstProperty(BarleyConstants.SERVER_URL), 
		  configCtx);
    }

	public static String getServerURL(ServletContext servletContext, HttpSession httpSession,
	            ConfigurationContext configCtx) {
		String url;
		Object obj = httpSession.getAttribute(BarleyConstants.SERVER_URL);
		if (obj instanceof String) {
			// Server URL is present in the servlet session
			url = (String) obj;
		} else {
			url = (String) servletContext.getAttribute(BarleyConstants.SERVER_URL);
		}
		return getServerURL(url, configCtx);
	}
	
	private static String getServerURL(String url, ConfigurationContext configCtx) {
		String carbonMgtParam = "${carbon.management.port}";
		String mgtTransport = getManagementTransport();
		String returnUrl = url;
		if (returnUrl.indexOf(carbonMgtParam) != -1) {
			String httpsPort =
			BarleyUtils.getTransportPort(configCtx, mgtTransport) + "";
			returnUrl = returnUrl.replace(carbonMgtParam, httpsPort);
		}
		if (returnUrl.indexOf("${carbon.context}") != -1) {
			String context = ServerConfiguration.getInstance().getFirstProperty("WebContextRoot");
			if (context.equals("/")) {
				context = "";
			}
			returnUrl = returnUrl.replace("${carbon.context}", context);
		}
		return returnUrl;
	}
}
