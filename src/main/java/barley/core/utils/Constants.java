package barley.core.utils;

public final class Constants {

    public static final String CARBON_HOME = "carbon.home";
    public static final String CARBON_HOME_ENV = "CARBON_HOME";
    public static final String RUNTIME_PATH = "wso2.runtime.path";
    public static final String RUNTIME_PATH_ENV = "RUNTIME_PATH";
    public static final String RUNTIME = "wso2.runtime";
    public static final String CARBON_CONFIG_YAML = "carbon.yaml";
    public static final String DEPLOYMENT_CONFIG_YAML = "deployment.yaml";
    public static final String CONF_DIR = "conf";

    /**
     * Remove default constructor and make it not available to initialize.
     */
    private Constants() {
        throw new AssertionError("Trying to a instantiate a constant class");
    }

    /**
     * Default value if it is not set in sys prop/env.
     */
    public static class PlaceHolders {
        public static final String SERVER_KEY = "carbon-kernel";
        public static final String SERVER_NAME = "WSO2 Carbon Kernel";
        public static final String SERVER_VERSION = "5";
    }
}