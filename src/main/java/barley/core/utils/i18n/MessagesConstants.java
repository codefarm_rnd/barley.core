package barley.core.utils.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessagesConstants {
    public static final String PROJECT_NAME = "org.wso2.carbon.utils".intern();
    public static final String RESOURCE_NAME = "resource".intern();
    public static final Locale LOCALE = null;

    public static final String rootPackageName = "org.wso2.carbon.utils.i18n".intern();
    
    public static final ResourceBundle rootBundle =
            ProjectResourceBundle.getBundle(PROJECT_NAME,
                    rootPackageName,
                    RESOURCE_NAME,
                    LOCALE,
                    MessagesConstants.class.getClassLoader(),
                    null);
}
