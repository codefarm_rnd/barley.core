package barley.core.utils;

import org.apache.axis2.context.ConfigurationContext;

/**
 * This observer will be notified when a new Axis2 ConfigurationContext is created & populated
 * or destroyed
 */
public interface Axis2ConfigurationContextObserver {

    /**
     * This method will be notified just before creating a new ConfigurationContext for any tenant
     *
     * @param tenantId The ID of the tenant
     */
    void creatingConfigurationContext(int tenantId);

    /**
     * This method will be notified after a new Axis2 ConfigurationContext is created
     *
     * @param configContext  The newly created ConfigurationContext
     */
    void createdConfigurationContext(ConfigurationContext configContext);

     /**
     * Notification before a ConfigurationContext is terminated
     *
     * @param configCtx The ConfigurationContext which will be terminated
     */
    void terminatingConfigurationContext(ConfigurationContext configCtx);

    /**
     * Notification after a ConfigurationContext has been terminated
     *
     * @param configCtx The ConfigurationContext which was terminated
     */
    void terminatedConfigurationContext(ConfigurationContext configCtx);
}