package barley.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * 
 */
public class InputReader {
	private InputReader() {
	    //disable external instantiation
	}

    private static final int BYTE_ARRAY_SIZE = 256;

	public static String readInput() throws IOException {
        byte b [] = new byte [BYTE_ARRAY_SIZE];
        int i = System.in.read(b);
        String msg = "";
        if (i != -1) {
            msg = new String(b).substring(0, i - 1).trim();
        }
        return msg;
    }

    public static String readPassword(String prompt) throws IOException {
        PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String password = null;
        while (password == null || password.length() == 0) {
            password = new PasswordPrompt(prompt, out).getPassword(in);
        }
        return password;
    }
}