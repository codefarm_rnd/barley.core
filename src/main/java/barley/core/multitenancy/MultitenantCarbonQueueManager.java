package barley.core.multitenancy;

import org.wso2.carbon.queuing.CarbonQueue;

public interface MultitenantCarbonQueueManager {

    /**
     * Method to obtain a queue.
     *
     * @param name     the name of the queue.
     * @param tenantId the tenant's identifier.
     *
     * @return the corresponding queue.
     */
    public CarbonQueue<?> getQueue(String name, int tenantId);

}