/*
 *  Copyright (c) 2005-2009, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 *  WSO2 Inc. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */
/*
 *  Copyright (c) 2005-2009, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 *  WSO2 Inc. licenses this file to you under the Apache License,
 *  Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */
package barley.core.multitenancy.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.description.AxisService;
import org.apache.axis2.description.TransportOutDescription;
import org.apache.axis2.engine.AxisConfiguration;
import org.apache.axis2.transport.TransportListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import barley.core.MultitenantConstants;
import barley.core.ServerConstants;
import barley.core.context.PrivilegedBarleyContext;
import barley.core.multitenancy.MultitenantUtils;
import barley.core.utils.Axis2ConfigurationContextObserver;
import barley.core.utils.Utils;
import barley.user.api.Tenant;
import barley.user.api.TenantManager;

/**
 * Utility methods for Tenant Operations at Axis2-level.
 */
@SuppressWarnings("unused")
public final class TenantAxisUtils {

	private static final Log log = LogFactory.getLog(TenantAxisUtils.class);
	private static final String TENANT_CONFIGURATION_CONTEXTS = "tenant.config.contexts";
	private static final String TENANT_CONFIGURATION_CONTEXTS_CREATED = "tenant.config.contexts.created";
	// private static CarbonCoreDataHolder dataHolder =
	// CarbonCoreDataHolder.getInstance();
	private static Map<String, ReentrantReadWriteLock> tenantReadWriteLocks = new ConcurrentHashMap<String, ReentrantReadWriteLock>();

	private TenantAxisUtils() {
	}

	public static AxisConfiguration getTenantAxisConfiguration(String tenant, ConfigurationContext mainConfigCtx) {
		ConfigurationContext tenantConfigCtx = getTenantConfigurationContext(tenant, mainConfigCtx);
		if (tenantConfigCtx != null) {
			return tenantConfigCtx.getAxisConfiguration();
		}
		return null;
	}

	public static ConfigurationContext getTenantConfigurationContextFromUrl(String url,
			ConfigurationContext mainConfigCtx) {
		String tenantDomain = MultitenantUtils.getTenantDomainFromUrl(url);
		return getTenantConfigurationContext(tenantDomain, mainConfigCtx);
	}

	public static ConfigurationContext getTenantConfigurationContext(String tenantDomain,
			ConfigurationContext mainConfigCtx) {
		/*
		ConfigurationContext tenantConfigCtx;
		if (tenantReadWriteLocks.get(tenantDomain) == null) {
			synchronized (tenantDomain.intern()) {
				if (tenantReadWriteLocks.get(tenantDomain) == null) {
					tenantReadWriteLocks.put(tenantDomain, new ReentrantReadWriteLock());
				}
			}
		}
		Lock tenantReadLock = tenantReadWriteLocks.get(tenantDomain).readLock();
		try {
			tenantReadLock.lock();
			Map<String, ConfigurationContext> tenantConfigContexts = getTenantConfigurationContexts(mainConfigCtx);
			tenantConfigCtx = tenantConfigContexts.get(tenantDomain);
			if (tenantConfigCtx == null) {
				try {
					tenantConfigCtx = createTenantConfigurationContext(mainConfigCtx, tenantDomain);
				} catch (Exception e) {
					throw new RuntimeException("Cannot create tenant ConfigurationContext for tenant " + tenantDomain,
							e);
				}
			}
			tenantConfigCtx.setProperty(MultitenantConstants.LAST_ACCESSED, System.currentTimeMillis());
		} finally {
			tenantReadLock.unlock();
		}
		return tenantConfigCtx;
		*/
		return null;
	}

	/**
	 * @param url               will have pattern
	 *                          <some-string>/t/<tenant>/<service>?<some-params>
	 * @param mainConfigContext The main ConfigurationContext from the server
	 * @return The tenant's AxisService
	 * @throws org.apache.axis2.AxisFault If an error occurs while retrieving the
	 *         AxisService
	 */
	public static AxisService getAxisService(String url, ConfigurationContext mainConfigContext) throws AxisFault {
		String[] strings = url.split("/");
		boolean foundTenantDelimiter = false;
		String tenant = null;
		String service = null;
		for (String str : strings) {
			if (!foundTenantDelimiter && str.equals("t")) {
				foundTenantDelimiter = true;
				continue;
			}
			if (foundTenantDelimiter & tenant == null) {
				tenant = str;
				continue;
			}
			if (tenant != null) {
				if (service == null) {
					service = str;
				} else {
					service += "/" + str;
				}
			}
		}
		if (service != null) {
			service = service.split("\\?")[0];
			AxisConfiguration tenantAxisConfig = getTenantAxisConfiguration(tenant, mainConfigContext);
			if (tenantAxisConfig != null) {
				return tenantAxisConfig.getServiceForActivation(service);
			}
		}
		return null;
	}

	
	/**
     * Get all the tenant ConfigurationContexts
     *
     * @param mainConfigCtx Super-tenant Axis2 ConfigurationContext
     * @return the tenant ConfigurationContexts as a Map of "tenant domain -> ConfigurationContext"
     */
    @SuppressWarnings("unchecked")
    public static Map<String, ConfigurationContext>
    getTenantConfigurationContexts(ConfigurationContext mainConfigCtx) {
        Map<String, ConfigurationContext> tenantConfigContexts = (Map<String, ConfigurationContext>)
                mainConfigCtx.getProperty(TENANT_CONFIGURATION_CONTEXTS);
        if (tenantConfigContexts == null) {
            tenantConfigContexts = new ConcurrentHashMap<String, ConfigurationContext>();
            mainConfigCtx.setProperty(TENANT_CONFIGURATION_CONTEXTS, tenantConfigContexts);
        }
        return tenantConfigContexts;
    }
    
    
}
